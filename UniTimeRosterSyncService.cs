﻿using Newtonsoft.Json;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Diagnostics;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.RosterSync.Interfaces;
using StudioKit.RosterSync.UniTime.Classes;
using StudioKit.RosterSync.UniTime.Exceptions;
using StudioKit.RosterSync.UniTime.Models;
using StudioKit.RosterSync.UniTime.Utilities;
using StudioKit.TransientFaultHandling.Http;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.RosterSync.UniTime
{
	public class UniTimeRosterSyncService<TGroup, TMembership, TGroupCourseSection, TTerm, TContext, TUser>
		: IRosterSyncService<TGroup, TMembership, TGroupCourseSection, TTerm, TContext, TUser>
			where TGroup : class, IGroup<TMembership, TGroupCourseSection>, new()
			where TMembership : class, IMembership, new()
			where TGroupCourseSection : class, IGroupCourseSection, new()
			where TTerm : class, ITerm, new()
			where TContext : DbContext, IRosterSyncDbContext<TGroup, TMembership, TGroupCourseSection, TTerm>
			where TUser : class, IUser, new()
	{
		private readonly IRosterSyncServiceSettings _rosterSyncServiceSettings;
		private readonly TContext _rosterSyncDbContext;
		private readonly IErrorHandler _errorHandler;
		private readonly ILogger _logger;
		private readonly Func<TContext, IEnumerable<TUser>, int, Task<IEnumerable<TUser>>> _getOrAddUsersAsync;

		public UniTimeRosterSyncService(IRosterSyncServiceSettings rosterSyncServiceSettings,
				TContext rosterSyncDbContext,
				IErrorHandler errorHandler,
				ILogger logger,
				Func<TContext, IEnumerable<TUser>, int, Task<IEnumerable<TUser>>> getOrAddUsersAsync)
		{
			if (rosterSyncServiceSettings == null)
			{
				throw new ArgumentNullException(nameof(rosterSyncServiceSettings));
			}
			if (!ValidateServiceSettings(rosterSyncServiceSettings))
			{
				throw new ArgumentException(nameof(rosterSyncServiceSettings));
			}
			if (rosterSyncDbContext == null)
			{
				throw new ArgumentNullException(nameof(rosterSyncDbContext));
			}
			if (errorHandler == null)
			{
				throw new ArgumentNullException(nameof(errorHandler));
			}
			if (logger == null)
			{
				throw new ArgumentNullException(nameof(logger));
			}
			if (getOrAddUsersAsync == null)
			{
				throw new ArgumentNullException(nameof(getOrAddUsersAsync));
			}

			_rosterSyncServiceSettings = rosterSyncServiceSettings;
			_rosterSyncDbContext = rosterSyncDbContext;
			_errorHandler = errorHandler;
			_logger = logger;
			_getOrAddUsersAsync = getOrAddUsersAsync;
		}

		public async Task<ICourseSection> GetCourseSectionAsync(int classId, CancellationToken cancellationToken)
		{
			try
			{
				return await GetResultAsync<CourseSection>(UniTimeEndpoints.GetCourseSectionUrl(classId));
			}
			catch (Exception ex)
			{
				throw new UniTimeException($"UniTime GetCourseSection failed (classId = {classId}). {ex.Message}", ex);
			}
		}

		public async Task<ICourseSection> GetCourseSectionAsync(int classId)
		{
			return await GetCourseSectionAsync(classId, CancellationToken.None);
		}

		public async Task<IEnumerable<ICourseSection>> GetInstructorScheduleAsync(string instructorId, string termCode, CancellationToken cancellationToken)
		{
			try
			{
				var termReference = Helpers.GetTermReference(termCode);
				var instructorSchedule = await GetResultAsync<InstructorSchedule>(UniTimeEndpoints.GetInstructorScheduleUrl(instructorId, termReference));
				return instructorSchedule?.Classes.GroupBy(g => g.ClassId).Select(s => s.First()) ?? new List<CourseSection>();
			}
			catch (Exception ex)
			{
				throw new UniTimeException($"UniTime GetInstructorSchedule failed (instructorId = {instructorId}, termCode = {termCode}). {ex.Message}", ex);
			}
		}

		public async Task<IEnumerable<ICourseSection>> GetInstructorScheduleAsync(string instructorId, string termCode)
		{
			return await GetInstructorScheduleAsync(instructorId, termCode, CancellationToken.None);
		}

		public async Task SyncGroupAsync(int groupId, string memberRoleId, CancellationToken cancellationToken)
		{
			try
			{
				var group = await _rosterSyncDbContext.Groups.SingleOrDefaultAsync(g => g.Id == groupId && !g.IsDeleted, cancellationToken);
				if (group == null)
				{
					throw new ArgumentNullException(nameof(group));
				}
				var courseSections = group.CourseSections;
				// delete all members if any
				if (!courseSections.Any())
				{
					var allGroupMembers = group.UserRoles.Where(u => u.ExternalId != null && u.RoleId.Equals(memberRoleId));
					_rosterSyncDbContext.GroupUserRoles.RemoveRange(allGroupMembers);
				}
				else
				{
					var allEnrollments = await GetAllEnrollments(courseSections, groupId);
					var syncedUsers = (await _getOrAddUsersAsync(_rosterSyncDbContext, allEnrollments.ToList(), group.Id)).ToList();
					SyncGroupMemberships(_rosterSyncDbContext, group, syncedUsers, memberRoleId);
				}
				await _rosterSyncDbContext.SaveChangesAsync(cancellationToken);
			}
			catch (Exception e)
			{
				throw new UniTimeException($"UniTime SyncGroup failed (groupId = {groupId}). {e.Message}", e);
			}
		}

		public async Task SyncGroupAsync(int groupId, string memberRoleId)
		{
			await SyncGroupAsync(groupId, memberRoleId, CancellationToken.None);
		}

		public async Task SyncAllGroupsAsync(string memberRoleId, CancellationToken cancellationToken)
		{
			var groupIds = _rosterSyncDbContext.Groups
				.Where(g => !g.IsDeleted && g.CourseSections.Any())
				.Select(g => g.Id);
			var successGroupIds = new List<int>();
			var failedGroupIds = new List<int>();
			foreach (var groupId in groupIds)
			{
				try
				{
					await SyncGroupAsync(groupId, memberRoleId, cancellationToken);
					successGroupIds.Add(groupId);
				}
				catch (Exception e)
				{
					if (e is UniTimeDownException)
						throw;
					_logger.Error(e.Message);
					_errorHandler.CaptureException(e);
					failedGroupIds.Add(groupId);
				}
			}
			if (failedGroupIds.Any())
			{
				var message = $"SyncAllGroupsAsync failed for groups ({string.Join(", ", failedGroupIds)}) and finished groups ({string.Join(", ", successGroupIds)})";
				_logger.Error(message);
				_errorHandler.CaptureException(new UniTimeException(message));
			}
		}

		public async Task SyncAllGroupsAsync(string memberRoleId)
		{
			await SyncAllGroupsAsync(memberRoleId, CancellationToken.None);
		}

		public async Task SyncTermsAsync(CancellationToken cancellationToken)
		{
			try
			{
				var roles = await GetResultAsync<List<Role>>(UniTimeEndpoints.GetTermsUrl());
				var terms = _rosterSyncDbContext.Terms.ToList();
				foreach (var role in roles)
				{
					var termCode = Helpers.GetTermCode(int.Parse(role.Year), role.Term);
					var term = terms.SingleOrDefault(t => t.TermCode.Equals(termCode, StringComparison.OrdinalIgnoreCase));
					if (term != null)
					{
						term.TermCode = termCode;
						term.Name = $"{role.Term} {role.Year}";
						term.StartDate = role.EventBeginDate;
						term.EndDate = role.EventEndDate;
					}
					else
					{
						_rosterSyncDbContext.Terms.Add(new TTerm
						{
							TermCode = termCode,
							Name = $"{role.Term} {role.Year}",
							StartDate = role.EventBeginDate,
							EndDate = role.EventEndDate,
						});
					}
				}
				await _rosterSyncDbContext.SaveChangesAsync(cancellationToken);
			}
			catch (Exception e)
			{
				throw new UniTimeException($"UniTime SyncTerms failed. {e.Message}", e);
			}
		}

		public async Task SyncTermsAsync()
		{
			await SyncTermsAsync(CancellationToken.None);
		}

		private static bool ValidateServiceSettings(IRosterSyncServiceSettings serviceSettings)
		{
			return !string.IsNullOrWhiteSpace(serviceSettings.RosterSyncEndPointUrl)
				&& !string.IsNullOrWhiteSpace(serviceSettings.RosterSyncUsername)
				&& !string.IsNullOrWhiteSpace(serviceSettings.RosterSyncPassword);
		}

		private async Task<IEnumerable<TUser>> GetAllEnrollments(IEnumerable<TGroupCourseSection> courseSections, int groupId)
		{
			var allEnrollments = Enumerable.Empty<Enrollment>();
			foreach (var section in courseSections ?? Enumerable.Empty<TGroupCourseSection>())
			{
				var enrollments = await GetEnrollments(section.ExternalId);

				var enrollmentList = enrollments?.ToList() ?? new List<Enrollment>();
				if (enrollmentList.Any())
				{
					allEnrollments = allEnrollments.Union(enrollmentList, new EnrollmentComparer());
				}
			}

			// enumerate once
			var enrollmentsList = allEnrollments.ToList();

			// find enrollments without email, track it but do not break execution
			var badEnrollments = enrollmentsList.Where(e => e.Email == null).ToList();
			if (badEnrollments.Any())
			{
				var message =
					$"UniTime result has bad enrollments with missing email. GroupId = {groupId}, ExternalIds = [{string.Join(",", badEnrollments.Select(e => e.ExternalId))}]";
				_logger.Error(message);
				_errorHandler.CaptureException(new UniTimeInvalidJsonException(message));
			}

			return enrollmentsList.Select(e => new TUser
			{
				Uid = e.Email?.Substring(0, e.Email.IndexOf("@", StringComparison.Ordinal)),
				FirstName = e.FirstName,
				LastName = e.LastName,
				EmployeeNumber = e.ExternalId.Length < 10 ? e.ExternalId.PadLeft(10, '0') : e.ExternalId,
				UserName = e.Email,
				Email = e.Email
			});
		}

		private async Task<IEnumerable<Enrollment>> GetEnrollments(int classId)
		{
			return await GetResultAsync<IEnumerable<Enrollment>>(UniTimeEndpoints.GetEnrollmentUrl(classId));
		}

		private static void SyncGroupMemberships(TContext dbContext, TGroup group, ICollection<TUser> syncedUsers, string memberRoleId)
		{
			var syncedUserIds = syncedUsers
				.Select(u => u.Id)
				.Distinct(StringComparer.OrdinalIgnoreCase)
				.ToArray();
			var existingUserRoles = group.UserRoles
				.Where(u => u.RoleId.Equals(memberRoleId, StringComparison.OrdinalIgnoreCase))
				.ToList();
			var existingUserIds = existingUserRoles
				.Select(u => u.UserId)
				.Distinct(StringComparer.OrdinalIgnoreCase)
				.ToArray();

			// Members to be added!
			var addingUserIds = syncedUserIds.Except(existingUserIds, StringComparer.OrdinalIgnoreCase);
			var usersToBeAdded = syncedUsers.Where(u => addingUserIds.Contains(u.Id, StringComparer.OrdinalIgnoreCase)).ToList();
			if (usersToBeAdded.Any())
			{
				dbContext.GroupUserRoles.AddRange(
					usersToBeAdded.Select(user => new TMembership
					{
						GroupId = group.Id,
						ExternalId = user.EmployeeNumber,
						UserId = user.Id,
						RoleId = memberRoleId
					})
				);
			}

			// Members to be deleted!
			// [NOTE]: A member can be deleted if it has been added automatically via RosterSync.
			//         Such member has an ExternalId field set to some value.
			var deletingUserIds = existingUserIds.Except(syncedUserIds, StringComparer.OrdinalIgnoreCase);
			var userRolesToBeDeleted = existingUserRoles
				.Where(u => deletingUserIds.Contains(u.UserId, StringComparer.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(u.ExternalId))
				.ToList();
			if (userRolesToBeDeleted.Any())
			{
				dbContext.GroupUserRoles.RemoveRange(userRolesToBeDeleted);
			}

			// Members to be updated to synced!
			// [NOTE]: If the user is added manually and then received via RosterSync, then it will be converted to
			//         a synced user by setting its ExternalId field to proper value.
			var updatingUserIds = existingUserIds.Intersect(syncedUserIds, StringComparer.OrdinalIgnoreCase);
			var userRolesToBeUpdated = existingUserRoles
				.Where(u => updatingUserIds.Contains(u.UserId, StringComparer.OrdinalIgnoreCase) && string.IsNullOrWhiteSpace(u.ExternalId))
				.ToList();
			if (userRolesToBeUpdated.Any())
			{
				foreach (var userRole in userRolesToBeUpdated)
				{
					userRole.ExternalId = syncedUsers.Single(u => u.Id.Equals(userRole.UserId, StringComparison.OrdinalIgnoreCase))
						.EmployeeNumber;
				}
			}
		}

		#region HttpClient

		protected async Task<T> GetResultAsync<T>(string path)
		{
			var json = await GetApiResultAsync(path);
			var result = DeserializeResult<T>(json);
			return result;
		}

		protected async Task<string> GetApiResultAsync(string path)
		{
			var decryptedUsername =
				EncryptedConfigurationManager.TryDecryptSettingValue(_rosterSyncServiceSettings.RosterSyncUsername);
			var decryptedPassword =
				EncryptedConfigurationManager.TryDecryptSettingValue(_rosterSyncServiceSettings.RosterSyncPassword);
			var decryptedEndPointUrl =
				EncryptedConfigurationManager.TryDecryptSettingValue(_rosterSyncServiceSettings.RosterSyncEndPointUrl);

			string json;
			var credentials = Helpers.Base64Encode($"{decryptedUsername}:{decryptedPassword}");
			var builder = new Uri(decryptedEndPointUrl + path);

			_logger.Debug("Getting API result with path {0}", path);
			var client = new RetryingHttpClient();
			try
			{
				json = await client.GetStringAsync(builder, credentials).ConfigureAwait(false);
			}
			catch (HttpRequestException ex)
			{
				if (ex.Message.Contains("400"))
					return "";
				throw new UniTimeDownException("UniTime API down", ex);
			}
			_logger.Debug("Results fetched from API for {0}", builder.AbsolutePath);
			_logger.Debug("Result: {0}", json);
			return json;
		}

		private T DeserializeResult<T>(string json)
		{
			_logger.Debug("Deserializing json: {0}", json);
			try
			{
				return JsonConvert.DeserializeObject<T>(json);
			}
			catch (JsonReaderException e)
			{
				throw new UniTimeInvalidJsonException($"Invalid UniTime JSON: {json}", e);
			}
		}

		#endregion HttpClient
	}
}