﻿namespace StudioKit.RosterSync.UniTime.Utilities
{
	public static class UniTimeEndpoints
	{
		private const string TermsUrl = "/api/roles";
		private const string InstructorTeachingTermsUrl = "/api/roles?id={0}";
		private const string InstructorScheduleUrl = "/api/instructor-schedule?term={0}&id={1}";
		private const string EnrollmentUrl = "/api/enrollments?classId={0}";
		private const string CourseSectionUrl = "/api/class-info?classId={0}";

		public static string GetTermsUrl() => TermsUrl;

		public static string GetInstructorTeachingTermsUrl(int instructorId)
			=> string.Format(InstructorTeachingTermsUrl, instructorId);

		public static string GetInstructorScheduleUrl(string instructorId, string term)
			=> string.Format(InstructorScheduleUrl, term, instructorId);

		public static string GetEnrollmentUrl(int classId)
			=> string.Format(EnrollmentUrl, classId);

		public static string GetCourseSectionUrl(int classId)
			=> string.Format(CourseSectionUrl, classId);
	}
}