﻿using System.Collections.Generic;

namespace StudioKit.RosterSync.UniTime.Models
{
	public class InstructorSchedule
	{
		public IEnumerable<CourseSection> Classes { get; set; }
	}
}