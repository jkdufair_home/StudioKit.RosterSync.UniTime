﻿using StudioKit.RosterSync.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.RosterSync.UniTime.Models
{
	public class CourseSection : ICourseSection
	{
		public int ClassId { get; set; }
		public IEnumerable<Course> Course { get; set; }

		public int Id
		{
			get { return ClassId; }
			set { ClassId = value; }
		}

		public IEnumerable<ICourse> Courses
		{
			get { return Course; }
			set { Course = (IEnumerable<Course>)value; }
		}

		public string SectionNumber
		{
			get
			{
				// Parse full CRN, e.g. "18192-003", to get just the section number
				var crn = Course.FirstOrDefault()?.ClassSuffix;
				return string.IsNullOrWhiteSpace(crn) ? crn : crn.Substring(crn.IndexOf("-", StringComparison.Ordinal) + 1, 3);
			}
		}

		public string Subpart { get; set; }

		public string Fullpart
		{
			get
			{
				// source: https://www.purdue.edu/registrar/forms/schedule-type-classifications.html
				var displayName = Subpart;
				if (Subpart.Equals("Lec")) displayName = "Lecture";
				if (Subpart.Equals("Rec")) displayName = "Recitation";
				if (Subpart.Equals("Prs")) displayName = "Presentation";
				if (Subpart.Equals("Lab")) displayName = "Lab";
				if (Subpart.Equals("Lbp")) displayName = "Lab Prep";
				if (Subpart.Equals("Cln")) displayName = "Clinic";
				if (Subpart.Equals("Sd")) displayName = "Studio";
				if (Subpart.Equals("Ex")) displayName = "Experiential";
				if (Subpart.Equals("Res")) displayName = "Research";
				if (Subpart.Equals("Ind")) displayName = "Individual Study";
				if (Subpart.Equals("Dis")) displayName = "Distance Education";
				if (Subpart.Equals("Pso")) displayName = "PSO (Practice/Study/Observation)";
				return displayName;
			}
		}
	}
}